package com.db.microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springapp2Application {

	public static void main(String[] args) {
		SpringApplication.run(Springapp2Application.class, args);
	}

}
