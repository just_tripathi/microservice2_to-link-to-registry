package com.db.microservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringTestWS {
	@GetMapping(path = "/test", produces = "text/html")
	public String getSomething() {
		return "xxxxxx";
	}
}

